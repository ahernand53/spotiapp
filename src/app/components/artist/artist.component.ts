import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';
import { timeout } from 'rxjs/operators';


@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styles: [
  ]
})
export class ArtistComponent {

  artista: any = {};
  loading: boolean = true;
  topTracks: any[] = [];

  constructor(private router: ActivatedRoute,
              private spotify: SpotifyService) { 

    this.router.params.subscribe( params => {
      this.getArtist( params['id'] );
      this.getTopTracks( params['id'] );

    })
  }

  getArtist( id:string ) {

    this.spotify.getArtist(id)
      .subscribe( artist => {
          this.artista = artist;
          this.loading = false;
      })

  }

  getTopTracks( id:string ) {
    this.spotify.getTopTracks(id)
      .subscribe( topTracks => {
        this.topTracks = topTracks;
        console.log(topTracks);
      })
  }


}
